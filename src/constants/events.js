export const ITEM_EQUIPPED = 'ItemEquipped';
export const ITEM_UNEQUIPPED = 'ItemUnequipped';
export const ATTACK_CHANGED = 'AttackChanged';
export const DEFENSE_CHANGED = 'DefenseChanged';
export const ITEM_ADDED = 'ItemAdded';
export const ITEM_REMOVED = 'ItemRemoved';
export const INVENTORY_SIZE_CHANGED = 'InventorySizeChanged';
export const INVENTORY_CHANGED = 'InventoryChanged';