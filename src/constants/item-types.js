export const HELMET = 'helmet';
export const GLOVES = 'gloves';
export const BOOTS = 'boots';
export const WEAPON = 'weapon';
export const SHIELD = 'shield';
export const ALL = [HELMET, GLOVES, BOOTS, WEAPON, SHIELD];
export const HAND_HELD = [WEAPON, SHIELD];
export const ARMOR = [HELMET, GLOVES, BOOTS];