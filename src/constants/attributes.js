export const TYPE = 'type';
export const NAME = 'name';
export const ICON = 'icon';
export const ATTACK = 'attack';
export const DEFENSE = 'defense';
export const QUALITY = 'quality';