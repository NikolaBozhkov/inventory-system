export const UIState = {
  selectedItem: null,
  draggedItem: null,
  markedSlot: null,
  equippableSlots: [],
  get isMarkedEquippable() {
    return this.equippableSlots.indexOf(this.markedSlot) != -1;
  }
}