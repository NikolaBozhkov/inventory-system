import EventCenter from '../modules/event-center';
import * as events from '../constants/events';
import * as ids from '../constants/ids';
import * as classes from '../constants/classes';

// Represents the html elements of the UI
export const UI = {
  slots: {
    helmet: null,
    gloves: null,
    boots: null,
    leftHand: null,
    rightHand: null
  },
  attributes: {
    attack: null,
    defense: null
  },
  playerScreen: null,
  inventoryScreen: null,
  inventory: null,
  inventoryResizeIcon: null,
  inventorySearch: null,
  init() {
    this.slots.helmet = document.getElementById(ids.HELMET);
    this.slots.gloves = document.getElementById(ids.GLOVES);
    this.slots.boots = document.getElementById(ids.BOOTS);
    this.slots.leftHand = document.getElementById(ids.LEFT_HAND);
    this.slots.rightHand = document.getElementById(ids.RIGHT_HAND);

    this.attributes.attack = document.getElementById(ids.PLAYER_ATTACK);
    this.attributes.defense = document.getElementById(ids.PLAYER_DEFENSE);

    this.playerScreen = document.getElementById(ids.PLAYER_SCREEN);
    this.inventoryScreen = document.getElementById(ids.INVENTORY_SCREEN);
    this.inventory = document.getElementById(ids.INVENTORY);
    this.inventoryResizeIcon = this.inventoryScreen.querySelector('.' + classes.ICON_RESIZE);
    this.inventorySearch = document.getElementById(ids.INVENTORY_SEARCH);

    EventCenter.subscribe(events.ATTACK_CHANGED, ({ value }) => {
      this.attributes.attack.innerHTML = value;
    });

    EventCenter.subscribe(events.DEFENSE_CHANGED, ({ value }) => {
      this.attributes.defense.innerHTML = value;
    });
  }
}