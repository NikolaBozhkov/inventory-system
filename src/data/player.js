import EventCenter from '../modules/event-center';
import * as events from '../constants/events';

// Represents the Player data without UI attachments
export const Player = {
  _attack: 0,
  get attack() {
    return this._attack;
  },
  set attack(value) {
    this._attack = value;
    EventCenter.notify(events.ATTACK_CHANGED, { value });
  },

  _defense: 0,
  get defense() {
    return this._defense;
  },
  set defense(value) {
    this._defense = value;
    EventCenter.notify(events.DEFENSE_CHANGED, { value });
  },

  helmet: null,
  gloves: null,
  boots: null,
  leftHand: null,
  rightHand: null,

  inventory: [],
  _inventorySize: 0,
  get inventorySize() {
    return this._inventorySize;
  },
  set inventorySize(value) {
    if (value >= this.inventory.length) {
      this._inventorySize = value;
      EventCenter.notify(events.INVENTORY_SIZE_CHANGED, { value });
    }
  },

  addToInventory(item, shouldNotify = false) {
    this.inventory.push(item);
    EventCenter.notify(events.INVENTORY_CHANGED);
    
    if (shouldNotify) {
      EventCenter.notify(events.ITEM_ADDED, { item });
    }
  },
  removeFromInventory(item, shouldNotify = false) {
    let index = this.inventory.indexOf(item);
    if (index != -1) {
      this.inventory.splice(index, 1);
      EventCenter.notify(events.INVENTORY_CHANGED);

      if (shouldNotify) {
        EventCenter.notify(events.ITEM_REMOVED, { item });
      }
    }
  },

  onItemEquip({ item, slot }) {
    this[slot] = item;
    this.removeFromInventory(item);
    this.updateStats();
  },
  onItemUnequip({ item, slot }) {
    this[slot] = null;
    this.addToInventory(item);
    this.updateStats();
  },

  calculateProp(prop) {
    // Because of JS precision, calculate everything together for correct stats
    let equipment = [this.helmet, this.gloves, this.boots, this.leftHand, this.rightHand];
    return equipment.reduce((acc, item) => {
      if (item) {
        return acc + item[prop] * item.quality;
      } else {
        return acc;
      }
    }, 0).toFixed(2);
  },

  updateStats() {
    this.attack = +this.calculateProp('attack');
    this.defense = +this.calculateProp('defense');
  },

  init() {
    EventCenter.subscribe(events.ITEM_EQUIPPED, this.onItemEquip.bind(this));
    EventCenter.subscribe(events.ITEM_UNEQUIPPED, this.onItemUnequip.bind(this));
  }
}