import * as ids from './constants/ids';

export function getSlotType(slot) {
  let slotId = slot.getAttribute('id');
  if (slotId === ids.LEFT_HAND) {
    return 'leftHand';
  } else if (slotId === ids.RIGHT_HAND) {
    return 'rightHand';
  } else if (slotId === ids.HELMET) {
    return 'helmet';
  } else if (slotId === ids.GLOVES) {
    return 'gloves';
  } else if (slotId === ids.BOOTS) {
    return 'boots';
  }
}