const pathToIcons = require.context('./resources', true);
import * as itemTypes from './constants/item-types';

const MODIFIERS = ['Common', 'Rare', 'Mighty', 'Legendary', 'Epic'];
const OWNERS = ['Serpent', 'Warrior', 'Thief', 'Mage', 'Dragon'];

function randomFromArray(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function generateItems(number) {
  let res = [];

  for (let i = 0; i < number; i++) {
    let type = randomFromArray(itemTypes.ALL);
    let icon = `./${type}.svg`;

    let mod = randomFromArray(MODIFIERS);
    let owner = randomFromArray(OWNERS);
    let capitalizedType = type.charAt(0).toUpperCase() + type.slice(1);
    let name = `${mod} ${capitalizedType} of The ${owner}`;
    
    res.push({
      name,
      icon: pathToIcons(icon),
      attack: (Math.random() * 20 + 1).toFixed(2),
      defense: (Math.random() * 20 + 1).toFixed(2),
      quality: (Math.random() * 10 + 1).toFixed(2),
      type
    });
  }

  return res;
}