let EventCenter = (function () {
  let eventMap = new Map();

  function subscribe(event, func) {
    if (!event || !func) return;
    
    if (!eventMap.has(event)) {
      eventMap.set(event, []);
    }

    eventMap.get(event).push(func);

    // Return unsubscribe function
    return () => {
      let subscribers = eventMap.get(event);
      let index = subscribers.indexOf(func);
      subscribers.splice(index, 1);
      if (subscribers.length == 0) {
        eventMap.delete(event);
      }
    }
  }

  function notify(event, object) {
    if (!eventMap.has(event)) return;
    
    for (let func of eventMap.get(event)) {
      func.call(null, object);
    }
  }

  return {
    subscribe,
    notify
  }
})();

export default EventCenter;