import { UI } from '../data/ui';
import { Player } from '../data/player';
import ItemInputHandler from './item-input-handler';
import UIHandler from './ui-handler';
import FilterHandler from './filter-handler';
import InventoryHandler from './inventory-handler';
import { generateItems } from '../item-generator';

let Initializer = (function () {
  function init() {
    UI.init();
    UIHandler.init();
    Player.init();
    ItemInputHandler.init();
    FilterHandler.init();
    InventoryHandler.init();

    // Testing data
    let items = generateItems(50);
    Player.inventorySize = 200;
    items.forEach(item => Player.addToInventory(item, true));
  }

  return {
    init
  }
})();

export default Initializer;