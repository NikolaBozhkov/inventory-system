import { UI } from '../data/ui';
import EventCenter from './event-center';
import * as classes from '../constants/classes';
import * as ids from '../constants/ids';
import * as events from '../constants/events';

let UIHandler = (function () {
  let moveables = [];

  function bindMovable() {
    let selected = null;
    let initialX, initialY;
    let resizingInventory = false;

    // A bit dodgy way to move the screens
    // Gets the correct element to move
    // Allows clicking on the screen & header (not button)
    function getScreen(target) {
      if (moveables.indexOf(target) != -1) {
        return target;
      }

      // Check if header is pressed
      let parent = target.parentElement;
      if (parent && moveables.indexOf(parent) != -1) {
        return parent;
      } else if (parent) {
        // Check if element in header is pressed (without button, search)
        let parentParent = parent.parentElement;
        if (parentParent 
          && target.tagName.toLowerCase() !== 'button'
          && target.tagName.toLowerCase() !== 'input'
          && moveables.indexOf(parentParent) != -1) {
          return parentParent;
        }
      }

      return null;
    }

    document.addEventListener('mousedown', event => {
      initialX = event.clientX;
      initialY = event.clientY;

      if (event.target === UI.inventoryResizeIcon) {
        resizingInventory = true;
      } else {
        let screen = getScreen(event.target);
        if (!screen) return;
        selected = screen;

        // Set focused screen on top
        moveables.forEach(screen => {
          if (screen != selected) screen.style.zIndex = 0;
          else screen.style.zIndex = 100;
        });
      }
    });

    document.addEventListener('mouseup', event => {
      resizingInventory = false;
      if (!selected) return;

      // Place at new location
      let boundingRect = selected.getBoundingClientRect();
      selected.style.top = boundingRect.top + 'px';
      selected.style.left = boundingRect.left + 'px';
      selected.style.transform = '';
      selected = null;
    });

    document.addEventListener('mousemove', event => {
      if (!selected && !resizingInventory) return;

      let translateX = event.clientX - initialX;
      let translateY = event.clientY - initialY;

      if (resizingInventory) {
        let invScreenRect = UI.inventoryScreen.getBoundingClientRect();
        UI.inventoryScreen.style.width = invScreenRect.width + translateX + 'px';
        UI.inventoryScreen.style.height = invScreenRect.height + translateY + 'px';

        // Move with resize
        initialX += translateX;
        initialY += translateY;
      } else if (selected) {
        selected.style.transform = 
          `translate(${translateX}px, ${translateY}px)`;
      }
    });
  }

  function bindVisibilityEvents() {
    document.addEventListener('keydown', event => {
      if (event.target === UI.inventorySearch) return;

      if (event.key === 'i') {
        UI.inventoryScreen.classList.toggle(classes.HIDDEN);
      } else if (event.key === 'c') {
        UI.playerScreen.classList.toggle(classes.HIDDEN);
      }
    });

    let closePlayerScrBtn = document.getElementById(ids.CLOSE_PLAYER_SCREEN);
    closePlayerScrBtn.addEventListener('click', () => {
      UI.playerScreen.classList.add(classes.HIDDEN);
    });

    let closeInventoryScrBtn = document.getElementById(ids.CLOSE_INVENTORY_SCREEN);
    closeInventoryScrBtn.addEventListener('click', () => {
      UI.inventoryScreen.classList.add(classes.HIDDEN);
    });
  }

  function init() {
    moveables.push(UI.playerScreen);
    moveables.push(UI.inventoryScreen);
    bindMovable();
    bindVisibilityEvents();

    EventCenter.subscribe()
  }

  return {
    init
  }
})();

export default UIHandler;