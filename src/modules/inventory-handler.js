import { UI } from '../data/ui';
import { Player } from '../data/player';
import EventCenter from './event-center';
import * as events from '../constants/events';
import * as classes from '../constants/classes';
import * as ids from '../constants/ids';

let InventoryHandler = (function () {
  function addItem({ item, hasElement }) {
    let itemElem = document.createElement('x-item');
    itemElem.model = item;
    for (let slot of UI.inventory.children) {
      if (!slot.firstChild) {
        slot.appendChild(itemElem);
        break;
      }
    }
  }

  function removeItem({ item }) {
    for (let slot of UI.inventory.children) {
      if (slot.firstChild && slot.firstChild.model === item) {
        slot.removeChild(shot.firstChild);
        break;
      }
    }
  }

  function adjustSlotCount({ value }) {
    let currentCount = UI.inventory.children.length;

    // Remove slots
    if (currentCount > value) {
      let toRemoveCount = currentCount - value;
      for (let i = 0; i < toRemoveCount; i++) {
        // Find empty, guaranteed to find
        let emptySlot = UI.inventory.children[0];
        while (emptySlot.firstChild) {
          emptySlot = emptySlot.nextElementSibling;
        }

        UI.inventory.removeChild(emptySlot);
      }
    } else {
      // Add slots
      let toAddCount = value - currentCount;
      let fragment = document.createDocumentFragment();
      
      for (let i = 0; i < toAddCount; i++) {
        let itemSlot = document.createElement('div');
        itemSlot.classList.add(classes.INVENTORY_SLOT);
        fragment.appendChild(itemSlot);
      }

      UI.inventory.appendChild(fragment);
    }

    // Update count
    document.getElementById(ids.SLOTS_AVAILABLE).innerHTML = value;
  }

  function updateTakenSlots() {
    document.getElementById(ids.SLOTS_TAKEN).innerHTML = Player.inventory.length;
  }

  function init() {
    EventCenter.subscribe(events.ITEM_ADDED, addItem);
    EventCenter.subscribe(events.ITEM_REMOVED, removeItem);
    EventCenter.subscribe(events.INVENTORY_SIZE_CHANGED, adjustSlotCount);
    EventCenter.subscribe(events.INVENTORY_CHANGED, updateTakenSlots);
  }

  return {
    init
  }
})();

export default InventoryHandler;