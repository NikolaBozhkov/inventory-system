import { UI } from '../data/ui';
import EventCenter from './event-center';
import * as classes from '../constants/classes';
import * as events from '../constants/events';

let ItemMover = (function () {
  function moveItemToSlot(item, slot, preventSwap = false) {
    // Swap if necessary
    if (slot.children.length != 0 && !preventSwap) {
      let slotItem = slot.removeChild(slot.firstChild);
      item.parentElement.appendChild(slotItem);
    }
  
    item.parentElement.removeChild(item);
    slot.appendChild(item);
  }

  /*
  If slot is not defined, finds the first empty slot
  and puts the item there. Otherwise finds the first
  empty slot, moves it before the target slot and
  puts the item there.
  */
  function moveItemToInventory(item, slot) {
    if (!slot) {
      for (let child of UI.inventory.children) {
        if (child.children.length == 0) {
          moveItemToSlot(item, child);
          break;
        }
      }
    } else {
      let emptySlot;
      let next = slot.nextElementSibling;
      while (next) {
        if (!next.firstChild) {
          emptySlot = next;
          break;
        }

        next = next.nextElementSibling;
      }

      if (emptySlot) {
        emptySlot.parentElement.removeChild(emptySlot);
        moveItemToSlot(item, emptySlot);
        // Force remove class because of insert
        item.classList.remove(classes.SELECTED);
        slot.insertAdjacentHTML('beforebegin', emptySlot.outerHTML);
      }
    }
  }

  return {
    moveItemToSlot,
    moveItemToInventory
  }
})();

export default ItemMover;