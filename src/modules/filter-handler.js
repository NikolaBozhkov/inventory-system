import { UI } from '../data/ui';
import EventCenter from './event-center';
import * as attr from '../constants/attributes';
import * as classes from '../constants/classes';
import * as events from '../constants/events';
import * as ids from '../constants/ids';
import * as itemTypes from '../constants/item-types';

let FilterHandler = (function () {
  const ALL_FILTER = 'all';
  let currentFilterType = ALL_FILTER;
  let currentFilterTypeBtn;
  let currentFilterSearchTerm = '';

  function applyFilters() {
    let typeRes = filterByType(UI.inventory.children);
    let searchRes = filterBySearch(typeRes);
    for (let slot of searchRes) {
      if (slot.classList.contains(classes.HIDDEN)) {
        slot.classList.remove(classes.HIDDEN);
      }
    }
  }

  function filterBy(slots, predicate) {
    let res = [];
    for (let slot of slots) {
      let item = slot.firstChild;
      if (item && predicate(item)) {
        res.push(slot);
      } else {
        slot.classList.add(classes.HIDDEN);
      }
    }

    return res;
  }

  function filterByType(slots) {
    if (currentFilterType === ALL_FILTER) return slots;

    return filterBy(slots, item => {
      return item.getAttribute(attr.TYPE) === currentFilterType;
    });
  }

  function filterBySearch(slots) {
    let searchTerm = UI.inventorySearch.value.toLowerCase();
    if (currentFilterSearchTerm === searchTerm || searchTerm.length == 0) return slots;
    
    return filterBy(slots, item => {
      let itemName = item.getAttribute(attr.NAME);
      let itemType = item.getAttribute(attr.TYPE);
      return itemName.toLowerCase().indexOf(searchTerm) != -1
        || itemType.indexOf(searchTerm) != -1;
    });
  }

  function setFilterType(type, button) {
    if (type === currentFilterType) return;
    currentFilterType = type;
    currentFilterTypeBtn.classList.remove(classes.SELECTED);
    button.classList.add(classes.SELECTED);
    currentFilterTypeBtn = button;
    applyFilters();
  }
  
  function init() {
    UI.inventorySearch.addEventListener('input', () => {
      applyFilters();
    });

    let filterAllBtn = document.getElementById(ids.FILTER_ALL);
    filterAllBtn.addEventListener('click', event => {
      setFilterType(ALL_FILTER, event.currentTarget);
    });

    document.getElementById(ids.FILTER_HELMET).addEventListener('click', event => {
      setFilterType(itemTypes.HELMET, event.currentTarget);
    });

    document.getElementById(ids.FILTER_GLOVES).addEventListener('click', event => {
      setFilterType(itemTypes.GLOVES, event.currentTarget);
    });

    document.getElementById(ids.FILTER_BOOTS).addEventListener('click', event => {
      setFilterType(itemTypes.BOOTS, event.currentTarget);
    });

    document.getElementById(ids.FILTER_WEAPON).addEventListener('click', event => {
      setFilterType(itemTypes.WEAPON, event.currentTarget);
    });

    document.getElementById(ids.FILTER_SHIELD).addEventListener('click', event => {
      setFilterType(itemTypes.SHIELD, event.currentTarget);
    });

    EventCenter.subscribe(events.ITEM_EQUIPPED, () => {
      applyFilters();
    });

    EventCenter.subscribe(events.ITEM_UNEQUIPPED, () => {
      applyFilters();
    });

    currentFilterTypeBtn = filterAllBtn;
    filterAllBtn.classList.add(classes.SELECTED);
  }

  return {
    init
  }
})();

export default FilterHandler;