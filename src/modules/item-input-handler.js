import { UIState } from '../data/ui-state';
import EquipmentHandler from './equipment-handler';
import SlotHandler from './slot-handler';
import ItemMover from './item-mover';
import * as classes from '../constants/classes';

const ITEM_TAG = 'x-item';

let ItemInputHandler = (function () {
  let initialX, initialY;

  function select(item) {
    UIState.selectedItem = item;
    UIState.draggedItem = item.cloneNode(true);
    document.body.appendChild(UIState.draggedItem);
    //UIState.selectedItem.classList.add(classes.SELECTED);

    UIState.draggedItem.classList.add(classes.DRAGGED);
    UIState.draggedItem.style.position = 'absolute';
  
    // Fix initial position of clone
    let selectedRect = UIState.selectedItem.getBoundingClientRect();
    let draggedRect = UIState.draggedItem.getBoundingClientRect();
    initialX -= selectedRect.x - draggedRect.x;
    initialY -= selectedRect.y - draggedRect.y;
  }
  
  function deselect() {
    //UIState.selectedItem.classList.remove(classes.SELECTED);
    UIState.draggedItem.parentElement.removeChild(UIState.draggedItem);
    UIState.selectedItem = null;
    UIState.draggedItem = null;
  }

  function updateDragged(clientX, clientY) {
    let translateX = clientX - initialX;
    let translateY = clientY - initialY;
    UIState.draggedItem.style.transform = `translate(${translateX}px, ${translateY}px)`;
  }
  
  function handleDrop() {
    if (!UIState.markedSlot) return;
  
    if (EquipmentHandler.isEquipment(UIState.selectedItem)) {
      // If moving left/right hand, else unequip
      if (UIState.isMarkedEquippable) {
        ItemMover.moveItemToSlot(UIState.selectedItem, UIState.markedSlot);
      } else {
        EquipmentHandler.unequipItem(UIState.selectedItem, UIState.markedSlot);
      }
    } else {
      // If equiping item, else moving inside inventory
      if (SlotHandler.isEquipmentSlot(UIState.markedSlot)) {
        EquipmentHandler.equipItem(UIState.selectedItem, UIState.markedSlot);
      } else {
        ItemMover.moveItemToSlot(UIState.selectedItem, UIState.markedSlot);
      }
    }
  }

  function clearState() {
    deselect();
    SlotHandler.clearMarkedSlot();
    SlotHandler.clearEquippableSlots();
  }

  function onMouseDown(event) {
    if (event.target.tagName.toLowerCase() !== ITEM_TAG) return;
    
    initialX = event.clientX;
    initialY = event.clientY;
    select(event.target);
    updateDragged(event.clientX, event.clientY);
    SlotHandler.markEquippableSlots();
    
    if (event.detail >= 2) {
      if (EquipmentHandler.isEquipment(UIState.selectedItem)) {
        EquipmentHandler.unequipItem(UIState.selectedItem);
      } else {
        // Find the the first empty equippable slot
        // If all are filled places it in the first
        let firstEmpty = UIState.equippableSlots.find(slot => !slot.firstChild);
        let slot = firstEmpty ? firstEmpty : UIState.equippableSlots[0];
        EquipmentHandler.equipItem(UIState.selectedItem, slot);
      }

      clearState();
    }
  }

  function onMouseUp() {
    if (!UIState.selectedItem) return;

    handleDrop();
    clearState();
  }

  function onMouseMove(event) {
    if (!UIState.selectedItem) return;

    updateDragged(event.clientX, event.clientY);
  
    // Find if markable slot at the position and mark
    let elementsFromPoint = document.elementsFromPoint(event.clientX, event.clientY);
    SlotHandler.clearMarkedSlot();
    for (let element of elementsFromPoint) {
      if (SlotHandler.shouldMark(element)) {
        SlotHandler.markSlot(element);
        break;
      }
    }
  }

  function init() {
    document.addEventListener('mousedown', onMouseDown);
    document.addEventListener('mouseup', onMouseUp);
    document.addEventListener('mousemove', onMouseMove);
  }

  return {
    init
  }
})();

export default ItemInputHandler;