import { getSlotType } from '../util';
import EventCenter from './event-center';
import ItemMover from './item-mover';
import * as classes from '../constants/classes';
import * as events from '../constants/events';

let EquipmentHandler = (function () {

  function equipItem(item, slot) {
    let itemBeforeEquip = slot.firstChild;
    ItemMover.moveItemToSlot(item, slot);

    // If swapped items in UI, first unequipped
    if (itemBeforeEquip) {
      EventCenter.notify(events.ITEM_UNEQUIPPED, { 
        item: itemBeforeEquip.model,
        slot: getSlotType(slot)
      });
    }

    EventCenter.notify(events.ITEM_EQUIPPED, { 
      item: item.model,
      slot: getSlotType(slot)
    });
  }
  
  function unequipItem(item, slot) {
    let itemSlot = item.parentElement;

    if (slot) {
      // If slot is empty just moves the item
      // Otherwise reposition the inventory items
      if (!slot.firstChild) {
        ItemMover.moveItemToSlot(item, slot);
      } else {
        ItemMover.moveItemToInventory(item, slot);
      }
    } else {
      ItemMover.moveItemToInventory(item);
    }

    EventCenter.notify(events.ITEM_UNEQUIPPED, {
      item: item.model,
      slot: getSlotType(itemSlot)
    });
  }
  
  function isEquipment(item) {
    return item.parentElement.classList.contains(classes.EQUIPMENT_SLOT);
  }

  return {
    equipItem,
    unequipItem,
    isEquipment
  }
})();

export default EquipmentHandler;