import { UI } from '../data/ui';
import { UIState } from '../data/ui-state';
import * as itemTypes from '../constants/item-types';
import * as classes from '../constants/classes';
import * as attr from '../constants/attributes';

let SlotHandler = (function () {
  function markSlot(element) {
    UIState.markedSlot = element;
    element.classList.add(classes.MARKED);
  }
  
  function clearMarkedSlot() {
    if (!UIState.markedSlot) return;
    UIState.markedSlot.classList.remove(classes.MARKED);
    UIState.markedSlot = null;
  }

  function markEquippableSlots() {
    let type = UIState.selectedItem.getAttribute(attr.TYPE);
    if (itemTypes.HAND_HELD.indexOf(type) != -1) {
      UIState.equippableSlots.push(UI.slots.leftHand, UI.slots.rightHand);
    } else if (itemTypes.ARMOR.indexOf(type) != -1) {
      UIState.equippableSlots.push(UI.slots[type]);
    }
  
    UIState.equippableSlots.forEach(slot => slot.classList.add(classes.EQUIPPABLE));
  }

  function clearEquippableSlots() {
    for (let equipmentSlot of UIState.equippableSlots) {
      equipmentSlot.classList.remove(classes.EQUIPPABLE);
    }
  
    UIState.equippableSlots = [];
  }

  function shouldMark(element) {
    // Don't mark if element is not a slot or contains the selected item
    if (element.firstChild && element.firstChild === UIState.selectedItem) return false;
    let isEquippable = isEquipmentSlot(element) && UIState.equippableSlots.indexOf(element) != -1;
    
    if (isInventorySlot(element) || isEquippable) {
      return true;
    }
    
    // Element is not a slot
    return false;
  }
  
  function isInventorySlot(slot) {
    return slot.classList.contains(classes.INVENTORY_SLOT);
  }

  function isEquipmentSlot(slot) {
    return slot.classList.contains(classes.EQUIPMENT_SLOT);
  }

  return {
    markSlot,
    clearMarkedSlot,
    markEquippableSlots,
    clearEquippableSlots,
    shouldMark,
    isInventorySlot,
    isEquipmentSlot
  }
})();

export default SlotHandler;