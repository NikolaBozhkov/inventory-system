let template = document.createElement('template');
template.innerHTML = getTemplateString();

class Item extends HTMLElement {
  constructor() {
    super();

    let shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(template.content.cloneNode(true));

    this.addEventListener('mouseover', event => {
      let info = this.shadowRoot.querySelector('.info');
      let itemRect = this.shadowRoot.host.getBoundingClientRect();
      
      info.style.bottom = window.innerHeight - itemRect.top + 'px';
      info.style.left = itemRect.left + 'px';
    });
  }

  get model() {
    return this._model;
  }

  set model(item) {
    this.setAttribute('name', item.name);
    this.setAttribute('icon', item.icon);
    this.setAttribute('quality', item.quality);
    this.setAttribute('attack', item.attack);
    this.setAttribute('defense', item.defense);
    this.setAttribute('type', item.type);

    this._model = item;
  }

  configureStat(prop) {
    let calcHolder = this.shadowRoot.querySelector(`.${prop} .calculated`);
    let baseHolder = calcHolder.nextSibling;
    calcHolder.innerHTML = '+' + (this[prop] * this.quality).toFixed(2);
    let capitalized = this.getCapitalized(prop);
    baseHolder.innerHTML = ` (${this[prop]}) ${capitalized}`;
  }

  configureIcon() {
    let iconHolders = this.shadowRoot.querySelectorAll('.icon');
    iconHolders.forEach(iconHolder => {
      iconHolder.setAttribute('src', this.getAttribute('icon'));
    });
  }

  configureName() {
    let nameHolder = this.shadowRoot.querySelector('.name');
    nameHolder.innerHTML = this.name;
  }

  configureQuality() {
    let qualityHolder = this.shadowRoot.querySelector('.quality');
    qualityHolder.innerHTML = this.quality + ' Quality';

    function setColors(color) {
      function setBorder(elem) {
        let rgbRes = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
        let r = parseInt(rgbRes[1], 16), g = parseInt(rgbRes[2], 16), b = parseInt(rgbRes[3], 16);
        elem.style.borderImage = `linear-gradient(to bottom, ${color} 0%, rgba(${r}, ${g}, ${b}, 0.3) 100%)`;
        elem.style.borderImageSlice = '1';
      }

      setBorder(this.shadowRoot.host);
      setBorder(this.shadowRoot.querySelector('.header .icon'));
      this.shadowRoot.querySelector('.name').style.color = color;
    }

    // configure border
    let color;
    if (this.quality < 2) {
      color = '#62A4DA';
    } else if (this.quality < 4) {
      color = '#1a9306';
    } else if (this.quality < 6) {
      color = '#fcd00b';
    } else if (this.quality < 8) {
      color = '#ffa405';
    } else {
      color = '#6719D3';
    }

    setColors.bind(this)(color);
    qualityHolder.style.color = color;
  }

  configureType() {
    let capitalized = this.getCapitalized(this.type);
    this.shadowRoot.querySelector('.type').innerHTML = capitalized;
  }

  getCapitalized(prop) {
    return prop.charAt(0).toUpperCase() + prop.slice(1);
  }

  static get observedAttributes() { 
    return ['name', 'icon', 'attack', 'defense', 'quality', 'type']; 
  }

  attributeChangedCallback(attr, oldValue, newValue) {
    if (oldValue !== newValue) {
      this[attr] = newValue;

      if (attr === 'name') {
        this.configureName();
      } else if (attr === 'icon') {
        this.configureIcon();
      } else if (attr === 'quality') {
        this.configureQuality();
      } else if (attr === 'attack') {
        this.configureStat('attack');
      } else if (attr === 'defense') {
        this.configureStat('defense');
      } else if (attr === 'type') {
        this.configureType();
      }
    }
  }
}

customElements.define('x-item', Item);

function getTemplateString() {
return `
<style>
:host {
  display: block;
  box-sizing: border-box;
}

:host, .header .icon {
  border: 1px solid transparent;
}

:host(.selected) {
  filter: brightness(0.4);
}

:host(.dragged) {
  z-index: 200;
}

:host(:not(.dragged):not(.selected)) > .icon:hover + .info {
  display: block;
  z-index: 10;
  animation: show 0.25s ease-out;
}

.info {
  display: none;
  position: fixed;
  background-color: rgba(25, 26, 31, 1);
  padding: 0.6rem;
  border: 1px solid #000;
}

.header {
  display: flex;
  align-items: center;
  margin-bottom: 0.2rem;
}

.header .icon {
  width: 25px;
  height: 25px;
  margin-right: 0.3rem;
}

.body {
  display: flex;
  flex-direction: column;
}

.body span {
  white-space: nowrap;
}

.calculated {
  color: lightgreen;
}

.type {
  display: block;
  color: #ccc;
  margin-top: 0.4rem;
}

@keyframes show {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}

</style>
<img class="icon" draggable="false" />
<div class="info">
  <div class="header">
    <img class="icon" />
    <span class="name"></span>
  </div>
  <div class="body">
    <span class="attack"><span class="calculated"></span><span></span></span>
    <span class="defense"><span class="calculated"></span><span></span></span>
    <span class="quality"></span>
    <span class="type"></span>
  </div>
</div>
`;
}